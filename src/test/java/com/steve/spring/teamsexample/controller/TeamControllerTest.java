package com.steve.spring.teamsexample.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.steve.spring.teamsexample.model.Team;
import com.steve.spring.teamsexample.service.TeamService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TeamController.class)
class TeamControllerTest {

  private static final Long TEAM_ID = 1L;
  private static final String TEAM_NAME = "Chelsea";
  private static final String TEAM_LOCATION = "London";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private TeamService service;

  ObjectMapper mapper = new ObjectMapper();

  @Test
  void getAllTeams() throws Exception {
    final Team team = new Team(TEAM_ID, TEAM_NAME, TEAM_LOCATION);
    when(service.getAllTeams()).thenReturn(Collections.singletonList(team));
    this.mockMvc
        .perform(get("/api/teams"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].id").value(TEAM_ID))
        .andExpect(jsonPath("$[0].name").value(TEAM_NAME))
        .andExpect(jsonPath("$[0].location").value(TEAM_LOCATION))
        .andReturn()
        .getResponse()
        .getContentAsString();
  }

  @Test
  void getTeamById() throws Exception {
    final Team team = new Team(TEAM_ID, TEAM_NAME, TEAM_LOCATION);
    when(service.getTeamById(TEAM_ID)).thenReturn(team);
    this.mockMvc
        .perform(get("/api/teams/{id}", "1"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(TEAM_ID))
        .andExpect(jsonPath("$.name").value(TEAM_NAME))
        .andExpect(jsonPath("$.location").value(TEAM_LOCATION))
        .andReturn()
        .getResponse()
        .getContentAsString();
  }

  @Test
  void createTeam() throws Exception {
    final Team teamToCreate = Team.builder().name(TEAM_NAME).location(TEAM_LOCATION).build();
    final Team createdTeam =
        Team.builder().name(teamToCreate.getName()).location(teamToCreate.getLocation()).build();
    when(service.createTeam(any(Team.class))).thenReturn(createdTeam);
    this.mockMvc
        .perform(
            post("/api/teams")
                .content(mapper.writeValueAsString(teamToCreate))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.name").value(teamToCreate.getName()))
        .andExpect(jsonPath("$.location").value(teamToCreate.getLocation()));
  }

  @Test
  void updateTeam() throws Exception {
    Team existingTeam = Team.builder().id(TEAM_ID).name(TEAM_NAME).location(TEAM_LOCATION).build();
    when(service.getTeamById(TEAM_ID)).thenReturn(existingTeam);
    existingTeam.setName("Chelsea FC");
    when(service.updateTeam(TEAM_ID, existingTeam)).thenReturn(existingTeam);
    this.mockMvc
        .perform(
            put("/api/teams/{id}", "1")
                .content(mapper.writeValueAsString(existingTeam))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("Chelsea FC"));
  }

  @Test
  void deleteTeam() throws Exception {
    this.mockMvc.perform(delete("/api/teams/{id}", "1")).andExpect(status().isOk());
  }
}