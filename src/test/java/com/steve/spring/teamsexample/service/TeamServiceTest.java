package com.steve.spring.teamsexample.service;

import com.steve.spring.teamsexample.model.Team;
import com.steve.spring.teamsexample.repository.TeamRepository;
import javassist.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class TeamServiceTest {

  private static final Long ACTUAL_TEAM_ID = 1L;
  private static final Long FALSE_TEAM_ID = 2L;

  @Mock
  private TeamRepository repository;
  @Mock
  private Team team;

  private TeamService service;

  @BeforeEach
  void setUp() {
    initMocks(this);
    service = new TeamService(repository);
  }

  @Test
  void getAllTeams() {
    when(repository.findAll()).thenReturn(Collections.singletonList(team));
    assertEquals(Collections.singletonList(team), service.getAllTeams());
  }

  @Test
  void getTeamById() throws NotFoundException {
    when(repository.findById(ACTUAL_TEAM_ID)).thenReturn(Optional.of(team));
    assertEquals(team, service.getTeamById(ACTUAL_TEAM_ID));
  }

  @Test
  void testNotFoundException() {
    assertThrows(NotFoundException.class, () -> service.getTeamById(FALSE_TEAM_ID));
  }

  @Test
  void createTeam() {
    when(repository.save(team)).thenReturn(team);
    assertEquals(team, service.createTeam(team));
  }

  @Test
  void updateTeam() throws NotFoundException {
    when(repository.findById(ACTUAL_TEAM_ID)).thenReturn(Optional.of(team));
    when(repository.save(team)).thenReturn(team);
    assertEquals(team, service.updateTeam(ACTUAL_TEAM_ID, team));
  }

  @Test
  void deleteTeam() {
    service.deleteTeam(ACTUAL_TEAM_ID);
    verify(repository).deleteById(ACTUAL_TEAM_ID);
  }
}