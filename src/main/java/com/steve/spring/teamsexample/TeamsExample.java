package com.steve.spring.teamsexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamsExample {

  public static void main(String[] args) {
    SpringApplication.run(TeamsExample.class, args);
  }
}
