package com.steve.spring.teamsexample.service;

import com.steve.spring.teamsexample.model.Team;
import com.steve.spring.teamsexample.repository.TeamRepository;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TeamService {

  private final TeamRepository repository;

  public TeamService(final TeamRepository repository) {
    this.repository = repository;
  }

  public List<Team> getAllTeams() {
    return repository.findAll();
  }

  public Team getTeamById(Long id) throws NotFoundException {
    final Optional<Team> team = repository.findById(id);
    if (team.isPresent()) {
      return team.get();
    } else {
      throw new NotFoundException("Team with id " + id + " could not be found");
    }
  }

  public Team createTeam(final Team team) {
    return repository.save(team);
  }

  public Team updateTeam(final Long id, final Team team) throws NotFoundException {
    final Team teamToBeUpdated = getTeamById(id);
    teamToBeUpdated.setName(team.getName());
    teamToBeUpdated.setLocation(team.getLocation());
    repository.save(teamToBeUpdated);
    return teamToBeUpdated;
  }

  public void deleteTeam(Long id) {
    repository.deleteById(id);
  }
}
