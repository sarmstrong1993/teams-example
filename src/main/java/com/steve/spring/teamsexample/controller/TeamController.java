package com.steve.spring.teamsexample.controller;

import com.steve.spring.teamsexample.model.Team;
import com.steve.spring.teamsexample.service.TeamService;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/teams")
public class TeamController {

  private final TeamService service;

  public TeamController(final TeamService service) {
    this.service = service;
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public Collection<Team> getAllTeams() {
    return service.getAllTeams();
  }

  @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Team getTeamById(@PathVariable("id") final Long id) throws NotFoundException {
    return service.getTeamById(id);
  }

  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Team createTeam(@RequestBody final Team team) {
    return service.createTeam(team);
  }

  @PutMapping(
      path = "{id}",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Team updateTeam(@PathVariable("id") final Long id, @RequestBody final Team team)
      throws NotFoundException {
    return service.updateTeam(id, team);
  }

  @DeleteMapping(path = "{id}")
  public void deleteTeam(@PathVariable("id") final Long id) {
    service.deleteTeam(id);
  }
}
